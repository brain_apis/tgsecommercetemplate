/* eslint-disable prettier/prettier */
import React, {PureComponent} from 'react';
import {View, StyleSheet, ScrollView, ActivityIndicator} from 'react-native';
import {Overlay, Text, Button, ListItem} from 'react-native-elements';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  actMarket,
  actCart,
  actState,
} from '../redux_file/actions/actionCreators';
import Icon from 'react-native-vector-icons/FontAwesome';

const EmptyPage = ({isLoading}) => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <Text>Keranjang kosong</Text>
    <Overlay isVisible={isLoading} width="auto" height="auto">
      <ActivityIndicator size="large" />
    </Overlay>
  </View>
);

function countTotalPrice(items, ids) {
  let prices = 0;

  ids.forEach(i => {
    prices += items[i].price;
  });
  return Math.round(prices);
}

class CartPage extends PureComponent {
  constructor(props) {
    super(props);

    props.requesting();
    // const prices = countTotalPrice(props.cart.byIds, props.cart.allIds);
    this.state = {
      username: props.auth.username,
      items: {},
      itemsId: [],
      prices: 0,
      nota: [
        {
          title: 'Sub-total',
          subtitle: '',
        },
        {
          title: 'Pajak',
          subtitle: '',
        },
        {
          title: 'Total',
          subtitle: '',
        },
      ],
    };
    this.setNota = this.setNota.bind(this);
    // console.log(props.app.isLoading, 'orfc');
  }

  _onCheckout() {
    this.props.requesting();
    this.props.send({
      token: this.props.auth.token,
      user_id: this.props.auth.user_id,
      items: this.state.items,
      amount: this.state.prices,
      itemsId: this.state.itemsId,
    });
    this.props.get(this.props.auth.user_id);
    this.setState({
      items: {},
      itemsId: [],
    });
  }

  setNota(prices) {
    this.setState({
      nota: [
        {
          title: 'Sub-total',
          subtitle: prices.toString(),
        },
        {
          title: 'Pajak',
          subtitle: this.props.market.tax + '%',
        },
        {
          title: 'Total',
          subtitle: (
            prices +
            (prices * this.props.market.tax) / 100
          ).toString(),
        },
      ],
    });
  }

  componentDidMount = () => {
    this.didFocus = this.props.navigation.addListener('didFocus', () => {
      this.props.navigation.setParams({
        itemCount: this.props.cart.byIds.length,
      });

      this.props.requestDone();
      this.setState(
        {
          items: this.props.cart.byIds,
          itemsId: this.props.cart.allIds,
        },
        () => {
          this.setNota(countTotalPrice(this.state.items, this.state.itemsId));
        },
      );
    });
    this.didBlur = this.props.navigation.addListener('didBlur', () => {
      this.props.cart_update(this.state.items);
      this.props.navigation.setParams({
        itemCount: this.state.itemsId.length,
      });
    });
  };

  componentWillUnmount = () => {
    this.didFocus.remove();
    this.didBlur.remove();
  };

  onRemoveFromCart = id => {
    const {items, itemsId} = this.state;
    delete items[id];

    // itemsId.splice(itemsId.indexOf(id), 1);
    let newIds = itemsId.filter(i => i != id);
    // console.log(newIds, items, 'orfc');
    const prices = countTotalPrice(items, newIds);

    this.setState({
      items: items,
      itemsId: newIds,
      prices: prices,
    });
    this.setNota(prices);
  };

  render() {
    return this.state.itemsId.length > 0 ? (
      <ScrollView>
        <ListItem title={`Daftar belanja (${this.state.itemsId.length})`} />
        {this.state.itemsId.map(i => (
          <ListItem
            key={i}
            leftAvatar={{source: {uri: this.state.items[i].item_image}}}
            title={this.state.items[i].item_name}
            subtitle={
              this.state.items[i].qty +
              ' x ' +
              (this.state.items[i].price / this.state.items[i].qty).toString()
            }
            bottomDivider
            rightIcon={
              <Icon.Button
                name="times"
                iconStyle={{color: 'red'}}
                backgroundColor="transparent"
                onPress={() => this.onRemoveFromCart(this.state.items[i].id)}
              />
            }
          />
        ))}
        <ListItem title={'Nota'} />
        {this.state.nota.map((l, i) => (
          <ListItem
            key={i}
            title={l.title}
            rightSubtitle={l.subtitle}
            bottomDivider
          />
        ))}
        <Button
          title="Pesan"
          containerStyle={styles.btnOrder}
          buttonStyle={{backgroundColor: 'indigo'}}
          onPress={() => this._onCheckout()}
        />
        <Overlay
          isVisible={this.props.app.isLoading}
          width="auto"
          height="auto">
          <ActivityIndicator size="large" />
        </Overlay>
      </ScrollView>
    ) : (
      <EmptyPage isLoading={this.props.app.isLoading} />
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    app: state.app,
    auth: state.auth,
    market: state.market,
    cart: state.cart,
  };
};
const mdtp = {
  send: actMarket.sendCheckout,
  get: actMarket.getOrderHistory,
  cart_update: actCart.updateCart,
  requesting: actState.requesting,
  requestDone: actState.loaded,
};

export default connect(mstp, mdtp)(CartPage);

const styles = StyleSheet.create({
  imgPromo: {
    // flex: 1,
    height: 200,
    width: 200,
    borderWidth: 2,
    borderColor: 'blue',
    marginBottom: 9,
  },
  contentPromo: {
    marginTop: 70,
  },

  itemPromo: {
    flex: 1,
    alignItems: 'center',
    marginTop: 5,
    marginHorizontal: 10,
    marginBottom: 10,
  },

  footer: {
    // flex: 1,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnOrder: {
    borderRadius: 30,
    width: wp(40),
    alignSelf: 'center',
    marginVertical: 50,
  },
});
