import {
  actState,
  actMarket,
  actAuth,
} from './../redux_file/actions/actionCreators';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  ActivityIndicator,
  Alert,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {Text} from 'react-native-elements';
import {Card, CardItem, Body} from 'native-base';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Icon from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';

const Skeleton = () => (
  <SafeAreaView>
    <StatusBar backgroundColor="#f0b729" barStyle="light-content" />
    <SkeletonPlaceholder>
      <View style={{width: '100%', height: hp(35)}} />
      <View
        style={{
          width: wp(35),
          height: 30,
          borderWidth: 5,
          borderColor: 'white',
          alignSelf: 'center',
          position: 'relative',
          marginTop: 20,
        }}
      />
      <View
        style={{
          width: wp(65),
          height: hp(20),
          alignSelf: 'center',
        }}
      />
      <View
        style={{
          width: wp(60),
          height: 20,
          alignSelf: 'center',
          marginTop: 12,
        }}
      />
      <View
        style={{
          width: wp(60),
          height: 20,
          alignSelf: 'center',
          marginTop: 12,
        }}
      />
    </SkeletonPlaceholder>
  </SafeAreaView>
);

class DisplayPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      reRequest: props.navigation.getParam('reRequest', ''),
    };
    props.navigation.setParams({
      reRequest: false,
    });
  }
  static navigationOptions = () => ({
    header: null,
  });

  _seeMore(item) {
    let cart = this.props.market.cart.filter(i => i.id === item.id);
    if (cart.length === 1) {
      Alert.alert(
        'Peringatan',
        'Item ini sudah ada di dalam keranjang, apa anda ingin mengubahnya ?',
        [
          {
            text: 'Ya',
            onPress: () => this.props.navigation.navigate('Detail', cart[0]),
          },
          {
            text: 'Tidak jadi',
            onPress: () => false,
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      this.props.navigation.navigate('Detail', item);
    }
  }

  componentDidMount = () => {
    this.didFocus = this.props.navigation.addListener('didFocus', () => {
      // console.log(this.props.auth.user_id, 'userid cdm');
      if (this.props.navigation.getParam('reRequest', false))
        this.props.getItems([this.props.auth.token]);
    });
  };

  render() {
    return this.props.app.isLoading ? (
      <Skeleton />
    ) : (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.scroll.scrollTo({x: 0, y: 0, animated: true});
          }}
          style={styles.fab}>
          <Icon name="chevrons-up" size={25} color="yellow" />
        </TouchableOpacity>
        <ScrollView
          ref={c => {
            this.scroll = c;
          }}>
          <Image
            source={require('./img/jumbotron1.jpeg')}
            style={{
              width: wp(100),
              height: hp(40),
              marginTop: wp(1),
              marginBottom: -10,
            }}
            PlaceholderContent={<ActivityIndicator />}
          />
          <FlatList
            data={this.props.market.items}
            keyExtractor={i => i.id.toString()}
            renderItem={({item}) => (
              <Card style={styles.itemPromo}>
                <CardItem style={{alignSelf: 'center', flexDirection: 'row'}}>
                  <Text h4>{item.item_name}</Text>
                  <Text
                    style={{
                      marginLeft: 20,
                      textDecorationLine: `${
                        item.nice ? 'none' : 'line-through'
                      }`,
                    }}>
                    Favorit
                  </Text>
                </CardItem>
                <CardItem button onPress={() => this._seeMore(item)}>
                  <Body
                    style={{
                      alignItems: 'center',
                    }}>
                    <Image
                      source={{uri: item.item_image}}
                      style={styles.imgPromo}
                      resizeMode="contain"
                    />
                    <Text>{item.description}</Text>
                    <Text>....</Text>
                  </Body>
                </CardItem>
              </Card>
            )}
          />
        </ScrollView>
      </View>
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    app: state.app,
    auth: state.auth,
    market: state.market,
  };
};

const mdtp = {
  getItems: actMarket.getItems,
  getToken: actAuth.getToken,
  requesting: actState.requesting,
  requestDone: actState.loaded,
};

export default connect(mstp, mdtp)(DisplayPage);

const styles = StyleSheet.create({
  imgPromo: {
    height: hp(25),
    width: wp(70),
    borderWidth: 2,
    borderColor: '#a2f02e',
    marginBottom: 9,
  },
  contentPromo: {
    marginTop: 70,
  },

  itemPromo: {
    // flex: 1,
    alignItems: 'center',
    marginTop: 5,
    margin: 15,
    marginBottom: 10,
    paddingVertical: 15,
    width: wp(95),
    alignSelf: 'center',
  },

  footer: {
    // flex: 1,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnDummy: {
    borderWidth: 1,
    width: wp(30),
    backgroundColor: 'red',
    borderColor: '#BCE0FD',
    borderRadius: 24,
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignItems: 'center',
  },

  fab: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.3)',
    alignItems: 'center',
    justifyContent: 'center',
    width: wp(12),
    height: wp(12),
    backgroundColor: '#fff',
    borderRadius: 50,
    position: 'absolute',
    bottom: 5,
    right: 5,
    zIndex: 999,
  },
});
