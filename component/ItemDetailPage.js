import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  TouchableHighlight,
  Text,
  Alert,
} from 'react-native';
import {Image} from 'react-native-elements';
import {Button} from 'native-base';
import {Divider} from './misc/PlugAndPlay';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {actMarket, actCart} from '../redux_file/actions/actionCreators';
import {RFPercentage} from 'react-native-responsive-fontsize';

class DisplayPage extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      item: props.navigation.state.params,
      count: 1,
    };

    this._fillCart = this._fillCart.bind(this);
  }

  static navigationOptions = {
    title: 'Halaman produk',
  };

  _fillCart() {
    let items = {...this.state.item};
    items.qty = this.state.count;
    items.price = items.price * items.qty;
    this.props.cart_add(items);

    Alert.alert('Info', 'Keranjang terisi, apakah itu item favoritmu ?', [
      {
        text: 'Ya',
        onPress: () => {
          this.props.navigation.goBack();
          this.props.like([this.props.auth.user_id, this.state.item.id]);
          this.props.navigation.navigate('Stage', {
            reRequest: true,
          });
        },
      },
      {
        text: 'Tidak juga',
        onPress: () => {
          this.props.navigation.goBack();
        },
      },
    ]);
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{position: 'absolute', alignSelf: 'center', top: hp(3)}}>
          <Text
            style={{
              fontWeight: '500',
              fontSize: RFPercentage(3),
              textDecorationLine: 'underline',
            }}>
            The {this.state.item.item_name}
          </Text>
        </View>
        <View style={styles.card}>
          <View style={{justifyContent: 'space-around'}}>
            <Text
              style={{
                width: wp(40),
                lineHeight: 17,
                textAlign: 'justify',
                marginTop: 10,
              }}
              numberOfLines={5}>
              {this.state.item.description}
            </Text>
            <View>
              <Text style={{color: 'grey'}}>Only</Text>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                {this.state.item.price}
              </Text>
            </View>
          </View>
          <View>
            <Image
              source={{
                uri: this.state.item.item_image,
              }}
              style={styles.imgProduct}
              PlaceholderContent={<ActivityIndicator />}
              resizeMode="contain"
            />
            <Button
              onPress={() => this._fillCart()}
              style={{
                height: hp(6),
                position: 'absolute',
                bottom: -10,
                right: 0,
                paddingHorizontal: 7,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  color: 'white',
                  fontSize: RFPercentage(2),
                }}>
                Tambah ke keranjang
              </Text>
            </Button>
          </View>
        </View>
        <View style={styles.receipt}>
          <Text style={{letterSpacing: 2, marginBottom: 5}}>DETIL ITEM</Text>
          <View
            style={{
              flexDirection: 'row',
              position: 'absolute',
              right: 10,
              top: -5,
            }}>
            <TouchableHighlight
              style={{
                borderRadius: 8,
                borderWidth: 1,
                padding: 2,
                paddingHorizontal: 8,
              }}
              onPress={() => this.setState({count: 1})}>
              <Text>reset</Text>
            </TouchableHighlight>
          </View>
          <Divider color="#CFCFCF" dvWidth={90} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'stretch',
            }}>
            <View>
              <Text>Harga</Text>
              <Text>Stok</Text>
              <Text>Pajak</Text>
              <View style={{flexDirection: 'row'}}>
                <Text>Jumlah</Text>
              </View>
            </View>
            <View>
              <Text>
                {Math.round(
                  this.state.item.price * this.state.count +
                    (this.state.item.price *
                      this.state.count *
                      this.props.market.tax) /
                      100,
                )}
              </Text>
              <Text>{this.state.item.qty}</Text>
              <Text>{this.props.market.tax} %</Text>
              <View style={{flexDirection: 'row'}}>
                <TouchableHighlight
                  style={{...styles.incButton, left: wp(-16)}}
                  onPress={() =>
                    this.state.count < this.state.item.qty &&
                    this.setState({count: this.state.count + 1})
                  }>
                  <Text>+</Text>
                </TouchableHighlight>
                <Text>{this.state.count}</Text>
                <TouchableHighlight
                  style={{...styles.incButton, left: wp(4)}}
                  onPress={() =>
                    this.state.count !== 0 &&
                    this.setState({count: this.state.count - 1})
                  }>
                  <Text>-</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    app: state.app,
    auth: state.auth,
    market: state.market,
  };
};

const mdtp = {
  cart_add: actCart.add,
  like: actMarket.like,
};

export default connect(mstp, mdtp)(DisplayPage);

const styles = StyleSheet.create({
  imgProduct: {
    width: wp(30),
    height: hp(30),
    // borderWidth: 2,
    borderColor: 'darkorchid',
  },
  card: {
    margin: hp(2),
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    height: hp(40),
  },
  receipt: {
    alignItems: 'center',
    paddingHorizontal: 50,
    marginBottom: 20,
    marginTop: 20,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'skyblue',
    flexDirection: 'row',
    height: hp(10),
    width: wp(100),
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  incButton: {
    width: wp(10),
    borderWidth: 1,
    borderRadius: 40,
    alignItems: 'center',
    marginHorizontal: 10,
    position: 'absolute',
  },
});
