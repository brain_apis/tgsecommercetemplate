/* eslint-disable prettier/prettier */
import React, {PureComponent, useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableNativeFeedbackComponent,
  Image,
  ScrollView,
} from 'react-native';
import {Text, PricingCard, ListItem, Button} from 'react-native-elements';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {actMarket, actAuth} from '../redux_file/actions/actionCreators';
import {RFPercentage} from 'react-native-responsive-fontsize';

const EmptyPage = ({status}) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>{status ? 'Tunggu sebentar ...' : 'Riwayat kosong : ('}</Text>
    </View>
  );
};

const Color = ['lightgreen', 'skyblue', 'green'];
const statusConst = ['In process', 'On the way', 'Shipped'];
const buttonString = ['LUNASI', 'PAKAI BUROQ AGAR CEPAT', 'TERKIRIM'];
// touch to ?
const touchType = [0, 1, 7];

function MyList({data, orderStatus, touch, user_id}) {
  const [visible, setVisible] = useState(false);
  const date = new Date(data.created_at);
  const statusIndex = statusConst.indexOf(orderStatus);
  return (
    <View>
      <ListItem
        title={`${date.getFullYear()}-${date.getMonth()}-${date.getDate()} / ${orderStatus} / ${
          data.detail.length
        } item`}
        bottomDivider
        chevron={!visible}
        onPress={() => setVisible(!visible)}
      />

      {visible && (
        <PricingCard
          color={Color[statusIndex]}
          title="Nilai"
          price={'Rp. ' + data.amount}
          pricingStyle={{fontSize: RFPercentage(5)}}
          info={data.detail.map(
            i => `${i.item_name} x ${i.qty} @${i.priceEach}`,
            // i => `${i.item_name.item_name} x ${i.qty} @${i.item_name.price}`,
          )}
          button={{title: buttonString[statusIndex]}}
          onButtonPress={() => {
            touch({
              touchType: touchType[statusIndex + 1],
              order_id: data.id,
              user_id: user_id,
              returnMessage: 'touch test ' + buttonString[statusIndex],
            });
            setVisible(false);
          }}
        />
      )}
    </View>
  );
}

class OrderHistory extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      nota: [],
      items: this.props.market.orderHistory,
      touch_order: this.props.market.touch_order,
      finding: true,
    };
  }

  componentDidMount = () => {
    // this.props.getToken(['jarvis', '1']);

    this.navEvent = this.props.navigation.addListener('didFocus', () => {
      this.props.get(this.props.auth.user_id);
      setTimeout(e => {
        this.setState({
          finding: false,
        });
      }, 3000);
    });
  };

  componentWillUnmount = () => {
    this.navEvent.remove();
  };

  render() {
    // console.log(this.props.market, 'render market');
    return this.props.market.orderHistory.length > 0 ? (
      <ScrollView>
        <Button
          buttonStyle={{backgroundColor: 'white'}}
          icon={{name: 'refresh', type: 'foundation', color: 'green'}}
          title="perbarui"
          titleStyle={{color: 'grey'}}
          onPress={() => {
            this.props.get(this.props.auth.user_id);
          }}
        />
        <View style={{padding: 10, elevation: 10}}>
          {this.props.market.orderHistory.map((item, i) => (
            <MyList
              key={i}
              data={item}
              orderStatus={item.status}
              touch={this.props.touch_order}
              user_id={this.props.auth.user_id}
            />
          ))}
        </View>
      </ScrollView>
    ) : (
      <EmptyPage status={this.state.finding} />
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    app: state.app,
    auth: state.auth,
    market: state.market,
  };
};

const mdtp = {
  get: actMarket.getOrderHistory,
  touch_order: actMarket.touch_order,
  getToken: actAuth.getToken,
};

export default connect(mstp, mdtp)(OrderHistory);
