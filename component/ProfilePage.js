/* eslint-disable prettier/prettier */
import React, {PureComponent} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {ListItem} from 'react-native-elements';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {actState, actMarket} from '../redux_file/actions/actionCreators';

class ProfilePage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cartVisible: false,
    };
    this.nota = [
      {
        title: 'Sub-total',
        subtitle: '10000',
      },
      {
        title: 'Pajak',
        subtitle: this.props.market.tax + '%',
      },
    ];
  }

  static navigationOptions = () => ({
    title: 'Welcome',
    // headerLeft: null,
    headerTitleStyle: {textAlign: 'center', flex: 1},
  });

  _seeMore(item) {
    let cart = this.props.market.cart.filter(i => i.id === item.id);
    if (cart.length === 1) {
      Alert.alert(
        'Peringatan',
        'Item ini sudah ada di dalam keranjang, apa anda ingin mengubahnya ?',
        [
          {
            text: 'Ya',
            onPress: () => this.props.navigation.navigate('Detail', cart),
          },
          {
            text: 'Tidak jadi',
            onPress: () => false,
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    } else {
      this.props.navigation.navigate('Detail', item);
    }
  }

  componentDidMount = () => {
    this.props.getItems([this.props.auth.token, '']);
    this.props.navigation.setParams({
      showCart: () => this.setState({cartVisible: true}),
    });
    this.navEvent = this.props.navigation.addListener('didFocus', () => {
      this.props.navigation.setParams({
        cartValue: this.props.market.cart.length,
      });
    });
  };

  componentWillUnmount = () => {
    this.navEvent.remove();
  };

  render() {
    return (
      <View>
        <ListItem title={'Judul'} />
        {this.props.market.items.map((l, i) => (
          <ListItem
            key={i}
            leftAvatar={{source: {uri: l.item_image}}}
            title={l.item_name}
            subtitle={l.qty + 'x' + l.price}
            bottomDivider
          />
        ))}
        <ListItem title={'Nota'} />
        {this.nota.map((l, i) => (
          <ListItem
            key={i}
            title={l.title}
            rightSubtitle={l.subtitle}
            bottomDivider
          />
        ))}
        {/* <FlatList
          data={this.props.market.items}
          keyExtractor={i => i.id.toString()}
          renderItem={({item}) => (
            <View>
              <Image
                source={{uri: item.item_image}}
                style={{width: 70, height: 70}}
              />
              <Text>{item.item_name}</Text>
              <Text>
                {item.qty} x {item.price}{' '}
              </Text>
              <Text>{item.qty * item.price} </Text>
            </View>
            // listitem rn elemens
            // sub total
            // tax
            // total
            // tombol chekcout
          )}
        /> */}
      </View>
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    app: state.app,
    auth: state.auth,
    market: state.market,
  };
};

const mdtp = {
  getItems: actMarket.getItems,
  requesting: actState.requesting,
  requestDone: actState.loaded,
};

export default connect(mstp, mdtp)(ProfilePage);
