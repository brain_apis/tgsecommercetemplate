/* eslint-disable prettier/prettier */
import React, {PureComponent} from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-elements';
import {connect} from 'react-redux';
import {actAuth} from '../../redux_file/actions/actionCreators';

class LogoutPage extends PureComponent {
  constructor(props) {
    super(props);
    props.logout();
    props.navigation.navigate('Login');
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Halaman logout</Text>
      </View>
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    auth: state.auth,
  };
};

const mdtp = {
  logout: actAuth.logout,
};

export default connect(mstp, mdtp)(LogoutPage);
