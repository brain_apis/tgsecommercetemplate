import React, {PureComponent} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableHighlight,
  Text,
  ActivityIndicator,
  Alert,
  StatusBar,
} from 'react-native';
import {FormWrapper} from '../misc/Wrappers';
import {Overlay} from 'react-native-elements';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {actState, actAuth} from './../../redux_file/actions/actionCreators';
import NetInfo from '@react-native-community/netinfo';
import {persistor} from '../../redux_file';
import SplashScreen from 'react-native-splash-screen';
import {RFPercentage} from 'react-native-responsive-fontsize';

export class ShopLogin extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      password: '',
    };

    props.requesting();
    // console.log(props.auth, 'auth');
  }

  requestingToken() {
    const {name, password} = this.state;

    if (name + password === '') {
      Alert.alert(
        'Konfirmasi',
        'Formulir kosong ): , ingin menggunakan akun demo ?',
        [
          {
            text: 'ya',
            onPress: () => {
              this.props.requesting();
              this.props.getToken(['jarvis', '1']);
            },
          },
          {
            text: 'tidak, saya mau mendaftar',
            onPress: () => this.props.navigation.navigate('Register'),
          },
        ],
      );
      return;
    }
    this.props.requesting();
    this.props.getToken([name, password]);
  }

  connectionChecker = (first = true) => {
    NetInfo.fetch().then(state => {
      if (state.isConnected === false) {
        Alert.alert(
          'tidak ada internet',
          'Gagal menemukan sambungan internet',
          [
            {
              text: 'coba lagi',
              onPress: () => {
                this.connectionChecker(false);
              },
            },
          ],
        );
      } else {
        if (!first) {
          Alert.alert(
            'Pesan',
            `Ok, berhasil menghubungkan ke koneksi ${state.type} :)`,
          );
        }
      }
    });
  };

  componentDidMount() {
    this.connectionChecker();
    // console.log(this.props.user, 'cdm login');
    if (this.props.user.user_id === '') {
      persistor.purge();
      this.props.requestDone();
    } else {
      // this.props.navigation.navigate('Main');
      setTimeout(() => this.props.navigation.navigate('Main'), 2000);
    }
    SplashScreen.hide();
  }

  render() {
    return (
      <FormWrapper>
        <StatusBar backgroundColor="#f0b729" barStyle="light-content" />
        <View style={styles.jumbotron}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: RFPercentage(8),
              color: 'brown',
            }}>
            Secret Shop
          </Text>
        </View>
        <View style={styles.formWrapper}>
          <TextInput
            style={styles.txtInput}
            placeholder="masukkan nama"
            onChangeText={name => this.setState({name})}
          />
          <TextInput
            style={styles.txtInput}
            secureTextEntry
            placeholder="password"
            onChangeText={password => this.setState({password})}
          />
        </View>
        <View style={styles.buttonset}>
          <TouchableHighlight
            style={{...styles.button, backgroundColor: '#428536'}}
            underlayColor="#fff"
            onPress={() => this.requestingToken()}>
            <Text style={styles.btnLogin}>L O G I N</Text>
          </TouchableHighlight>
          {/* <TouchableHighlight style={styles.button}>
            <Text>Masuk menggunakan Google</Text>
          </TouchableHighlight> */}
        </View>
        <Overlay
          isVisible={this.props.app.isLoading}
          width="auto"
          height="auto">
          <ActivityIndicator size="large" />
        </Overlay>
      </FormWrapper>
    );
  }
}
const mstp = (state /*, ownProps*/) => {
  return {
    app: state.app,
    user: state.auth,
    redux_persist: state._persist,
    // isLoading: state.app.isLoading,
  };
};

const mdtp = {
  requesting: actState.requesting,
  requestDone: actState.loaded,
  getToken: actAuth.getToken,
};

export default connect(mstp, mdtp)(ShopLogin);

const styles = StyleSheet.create({
  jumbotron: {
    // height: hp(25),
    flex: 1,
    width: wp(100),
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#2699FB',
  },
  txtInput: {
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'white',
    borderRadius: 24,
    paddingVertical: 5,
    paddingHorizontal: 15,
    fontSize: 15,
    textAlign: 'center',
  },

  formWrapper: {
    width: wp(80),
    // flex: 3,
    height: hp(35),
    marginBottom: 10,
    justifyContent: 'space-evenly',
    // borderWidth: 1,
    marginHorizontal: 20,
  },

  buttonset: {
    flex: 2,
    justifyContent: 'space-around',
    // height: hp(15),
    marginBottom: 30,
  },

  button: {
    borderWidth: 1,
    borderColor: '#BCE0FD',
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignItems: 'center',
    width: wp(50),
    height: hp(6),
    justifyContent: 'center',
  },

  title: {
    textDecorationLine: 'underline',
    fontSize: 20,
    fontWeight: '600',
  },

  btnLogin: {
    fontSize: 15,
    color: 'white',
  },
});
