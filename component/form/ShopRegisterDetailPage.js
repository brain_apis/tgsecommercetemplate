import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableHighlight,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native';
import {FormWrapper} from '../misc/Wrappers';
import {Divider} from '../misc/PlugAndPlay';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {actAuth, actState} from '../../redux_file/actions/actionCreators';
import ImagePicker from 'react-native-image-picker';
import {Overlay} from 'react-native-elements';
import {RFPercentage} from 'react-native-responsive-fontsize';

const options = {
  title: 'pilih foto profil',
  customButtons: [{name: 'fb', title: 'ambil foto dari galeri'}],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export class ShopRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      age: '11',
      address: '11',
      imgResponse: '',
      img: {
        uri:
          'http://savings.gov.pk/wp-content/plugins/ldd-directory-lite/public/images/noimage.png',
        name: 'noimage.png',
        type: 'image/png',
      },
    };

    this.choosePhoto = this.choosePhoto.bind(this);
  }

  async onPressRegister() {
    const {params} = this.props.navigation.state;
    const body = {
      ...params,
      age: this.state.age,
      isOwner: true,
    };
    if (this.state.imgResponse !== '') {
      body.avatar = {
        name: this.state.imgResponse.fileName,
        uri: this.state.imgResponse.uri,
        type: 'image/jpg',
      };
    }
    this.props.register(body);
    this.props.requesting();
  }

  choosePhoto() {
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        console.log(response, 'choose photo response');
        this.setState({
          img: {uri: 'data:image/jpeg;base64,' + response.data},
          imgResponse: response,
        });
      }
    });
  }

  render() {
    return (
      <FormWrapper>
        <View style={styles.centerItem}>
          <Text style={{fontSize: RFPercentage(4), color: 'white'}}>
            Buat akun mu!
          </Text>
        </View>
        <Divider dvColor="white" dvWidth={65} stroke={2} />
        <View style={styles.formWrapper}>
          <View
            style={{
              justifyContent: 'space-evenly',
              alignItems: 'center',
              height: hp(30),
            }}>
            <TouchableHighlight onPress={this.choosePhoto}>
              <Image
                source={this.state.img}
                style={{width: 120, height: 120}}
              />
            </TouchableHighlight>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={{...styles.txtInput, flex: 3}}
              placeholder="masukkan alamat"
              onChangeText={address => this.setState({address})}
            />
            <TextInput
              style={{...styles.txtInput, flex: 1}}
              placeholder="umur"
              onChangeText={age => this.setState({age})}
            />
          </View>
        </View>
        <View style={styles.buttonset}>
          {/* <Divider dvWidth={80} /> */}
          <TouchableHighlight
            style={{...styles.button, backgroundColor: '#428536'}}
            onPress={() => this.onPressRegister()}>
            <Text style={{color: 'white'}}>D A F T A R</Text>
          </TouchableHighlight>
        </View>
        <Overlay isVisible={this.props.isLoading} width="auto" height="auto">
          <ActivityIndicator size="large" />
          <Text>Sedang mendaftarkan ...</Text>
        </Overlay>
      </FormWrapper>
    );
  }
}

const mstp = (state /*, ownProps*/) => {
  return {
    isLoading: state.app.isLoading,
  };
};

const mdtp = {
  register: actAuth.register,
  getToken: actAuth.getToken,
  requesting: actState.requesting,
};
export default connect(mstp, mdtp)(ShopRegister);

const styles = StyleSheet.create({
  txtInput: {
    borderBottomWidth: 1,
    borderColor: 'white',
    paddingVertical: 5,
    paddingHorizontal: 15,
    textAlign: 'center',
    marginHorizontal: 4,
  },

  formWrapper: {
    width: wp(70),
    flex: 4,
    // height: hp(35),
    marginBottom: 10,
    justifyContent: 'space-evenly',
    // borderWidth: 1,
    marginHorizontal: 20,
  },

  buttonset: {
    flex: 2,
    justifyContent: 'space-evenly',
    // height: hp(15),
    marginBottom: 30,
  },

  button: {
    borderWidth: 1,
    borderColor: '#BCE0FD',
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignItems: 'center',
    height: hp(5),
    width: wp(25),
    justifyContent: 'center',
  },

  title: {
    textDecorationLine: 'underline',
    fontSize: 20,
    fontWeight: '600',
  },
  centerItem: {
    flex: 1,
    justifyContent: 'center',
  },
});
