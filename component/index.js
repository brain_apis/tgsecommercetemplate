import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import ShopLogin from './form/ShopLoginPage';
import ShopRegister from './form/ShopRegisterPage';
import ShopRegisterDetail from './form/ShopRegisterDetailPage';
import Stage from './DisplayPage';
import CartPage from './CartPage.';
import ItemDetail from './ItemDetailPage';
import Checkout from './CheckoutPage';
import ExperimentPage from './Experiment';
import OrderHistoryPage from './OrderHistory';
import LogoutPage from './form/Logout';
import McIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Text, View} from 'react-native';
import CustomDrawerContentComponent from './misc/CustomDrawer';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const ReturnStack = createStackNavigator({
  Logout: LogoutPage,
});

const RegisterStack = createStackNavigator({
  RegisterStep1: ShopRegister,
  RegisterStep2: {
    screen: ShopRegisterDetail,
    navigationOptions: {
      title: 'Data tambahan',
      headerTitleStyle: {textAlign: 'center', flex: 1},
    },
  },
});
const AuthStack = createBottomTabNavigator(
  {
    Login: ShopLogin,
    Register: RegisterStack,
  },
  {
    style: {
      borderWidth: 0,
    },
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'brown',
      activeBackgroundColor: '#ffc852',
      inactiveBackgroundColor: '#fc9803',
      barStyle: {
        borderWidth: 0,
      },
      labelStyle: {
        fontSize: 20,
        marginBottom: 10,
      },
    },
  },
);

AuthStack.navigationOptions = {
  headerShown: false,
};

const MainStack = createStackNavigator(
  {
    Stage: {
      screen: Stage,
      params: {
        reRequest: true,
      },
    },
    Detail: ItemDetail,
    // Checkout: Checkout,
    // Experiment: ExperimentPage,
  },
  {
    // initialRouteName: 'Experiment',
    initialRouteName: 'Stage',
    // headerMode: 'none',
  },
);

const ShopBottomNav = createMaterialBottomTabNavigator(
  {
    Main: {
      screen: MainStack,
      navigationOptions: ({navigation}) => {
        return {
          title: 'Teras',
          tabBarIcon: (
            <McIcon name="home" size={35} style={{width: 50, top: -8}} />
          ),
        };
      },
    },
    OrderHistory: {
      // eslint-disable-next-line no-undef
      screen: OrderHistoryPage,
      navigationOptions: ({navigation}) => {
        return {
          title: 'Auth',
          tabBarIcon: (
            <McIcon name="drama-masks" size={35} style={{width: 50, top: -8}} />
          ),
        };

        // gng faham100;
      },
      edgeWidth: 200,
    },
    Cart: {
      screen: CartPage,
      params: {
        itemCount: 0,
      },
      navigationOptions: ({navigation}) => {
        return {
          title: 'Keranjang',
          tabBarIcon: (
            <View>
              <McIcon name="cart" size={35} style={{width: 50, top: -8}} />
              <Text
                style={{
                  position: 'absolute',
                  top: -10,
                  backgroundColor: 'gold',
                  borderRadius: 6,
                  width: 15,
                  height: 20,
                  padding: 1,
                  textAlign: 'center',
                }}>
                {navigation.state.params.itemCount}
              </Text>
            </View>
          ),
        };
      },
    },
    Logout: {
      screen: AuthStack,
      navigationOptions: ({navigation}) => {
        return {
          title: 'Auth',
          tabBarIcon: (
            <McIcon name="door-open" size={35} style={{width: 50, top: -8}} />
          ),
          tabBarOnPress: ({navigation}) => {
            navigation.openDrawer();
          },
        };
      },
    },
  },
  {
    barStyle: {
      height: 50,
      backgroundColor: 'gold',
    },
    labeled: false,
    activeColor: 'black',
    inactiveTintColor: 'gray',
    initialRouteName: 'Main',
  },
);

const ShopWithDrawer = createDrawerNavigator(
  {
    Main: ShopBottomNav,
    Logout: ReturnStack,
  },
  {
    drawerType: 'front',
    drawerPosition: 'right',
    hideStatusBar: true,
    contentComponent: CustomDrawerContentComponent,
    drawerWidth: wp(80),

    // gng faham
    minSwipeDistance: 100,
    edgeWidth: 200,
    // headerMode: 'none',
  },
);

const AppNavigator = createSwitchNavigator({
  Begin: AuthStack,
  Shop: ShopWithDrawer,
});
export default createAppContainer(AppNavigator);
