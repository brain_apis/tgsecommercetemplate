import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import {DrawerItems} from 'react-navigation-drawer';
import {connect} from 'react-redux';
import {actAuth} from '../../redux_file/actions/actionCreators';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {persistor} from '../../redux_file';

const CustomDrawerContentComponent = props => {
  const data = [
    ['Nama', props.auth.username],
    ['Email', props.auth.email],
    ['Umur', props.auth.age],
    ['Alamat', props.auth.address],
    ['Status', props.auth.isAdmin ? 'Administrator' : 'User'],
  ];
  // console.log(props.auth.avatar);
  return (
    <ScrollView>
      <SafeAreaView
        style={styles.container}
        forceInset={{top: 'always', horizontal: 'never'}}>
        <View style={styles.topSection}>
          <Text
            style={{
              fontSize: 30,
              fontWeight: 'bold',
              textAlign: 'center',
              letterSpacing: 2,
            }}>
            Profil
          </Text>
          <Image source={{uri: props.auth.avatar}} style={styles.avatar} />
          {data.map((item, i) => (
            <View
              key={i}
              style={{
                ...styles.listDataContainer,
                backgroundColor: i % 2 === 1 ? 'white' : 'grey',
              }}>
              <Text style={{fontSize: 16}}>{item[0]}</Text>
              <Text style={{left: 70, position: 'absolute'}}>:</Text>
              <Text style={{fontSize: 16}}>{item[1]}</Text>
            </View>
          ))}
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            Alert.alert('Konfirmasi', 'Anda akan keluar', [
              {
                text: 'OK',
                onPress: () => {
                  persistor.purge().then(i => {
                    // console.log(i, 'purged');
                    props.logout();
                    props.navigation.navigate('Begin');
                  });
                },
              },
              {text: 'Tidak jadi'},
            ]);
          }}>
          <Text style={{color: '#f8fa84'}}>K E L U A R</Text>
        </TouchableOpacity>
        {/* <DrawerItems itemStyle={{justifyContent: 'center'}} {...props} /> */}
      </SafeAreaView>
    </ScrollView>
  );
};

const mstp = (state /*, ownProps*/) => {
  return {
    auth: state.auth,
  };
};
const mdtp = {
  logout: actAuth.logout,
};
export default connect(mstp, mdtp)(CustomDrawerContentComponent);

const drawerWidth = wp(85);
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: hp(100),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f8fa84',
  },
  listDataContainer: {
    flexDirection: 'row',
    width: drawerWidth,
    height: hp(5),
    padding: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listData: {alignItems: 'stretch'},
  button: {
    borderWidth: 1,
    borderColor: '#333',
    borderRadius: 5,
    position: 'absolute',
    bottom: 40,
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignItems: 'center',
    width: drawerWidth / 2,
    height: hp(6),
    justifyContent: 'center',
    elevation: 2,
    backgroundColor: '#bd2626',
  },
  topSection: {
    position: 'absolute',
    top: hp(2),
    height: hp(80),
  },
  avatar: {
    width: wp(33),
    height: hp(20),
    borderRadius: 75,
    alignSelf: 'center',
    margin: 30,
    borderWidth: 1,
    borderColor: 'black',
  },
});
