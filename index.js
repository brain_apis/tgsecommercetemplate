/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React from 'react';
import {Provider} from 'react-redux';
import {store, persistor} from './redux_file';
import {PersistGate} from 'redux-persist/integration/react';
import 'react-native-gesture-handler';
// import {SplashScreen} from './component/SplashScreen';

const AppLauncher = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent(appName, () => AppLauncher);
