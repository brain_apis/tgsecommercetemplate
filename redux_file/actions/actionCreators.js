import {authActions, appState, marketTrans, carts} from './actionTypes';

export const actAuth = {
  tokenGranted: payload => ({
    type: authActions.tokenGranted,
    ...payload,
  }),
  logout: () => ({
    type: authActions.logout,
  }),
  getToken: payload => ({
    type: authActions.getToken,
    payload: {
      username: payload[0],
      password: payload[1],
    },
  }),
  register: payload => {
    const data = new FormData();
    data.append('name', payload.name);
    data.append('email', payload.email);
    data.append('password', payload.password);
    data.append('isOwner', payload.isOwner);
    data.append('avatar', payload.avatar);
    data.append('age', payload.age);
    // console.log(data, 'fdata');
    return {
      type: authActions.doRegister,
      payload: {
        body: data,
      },
    };
  },
};

export const actState = {
  requesting: () => ({
    type: appState.requesting,
  }),

  loaded: () => ({
    type: appState.loaded,
  }),
};

export const actMarket = {
  getItems: payload => ({
    type: marketTrans.getItems,
    payload: {
      token: payload[0],
    },
  }),
  sendCheckout: payload => ({
    type: marketTrans.sendCheckout,
    payload: {
      token: payload.token,
      body: {
        user_id: payload.user_id,
        amount: payload.amount,
        items: payload.itemsId.map(i => ({
          item_id: i,
          qty: payload.items[i].qty,
        })),
      },
    },
  }),

  getOrderHistory: payload => ({
    type: marketTrans.getOrderHistory,
    payload: {
      id: payload,
    },
  }),

  like: payload => ({
    type: marketTrans.likesItem,
    user_id: payload[0],
    payload: {
      body: {
        item_id: payload[1],
      },
    },
  }),

  touch_order: payload => ({
    type: marketTrans.touch_order,
    order_id: payload.order_id,
    user_id: payload.user_id,
    returnMessage: payload.returnMessage,
    payload: {
      body: {
        type: payload.touchType,
      },
    },
  }),
};

export const actCart = {
  removeFromCart: payload => ({
    type: carts.removeItemFromCart,
    payload: {
      id: payload,
    },
  }),
  updateCart: payload => ({type: carts.updateCart, payload: payload}),
  add: payload => ({
    type: carts.add,
    payload: payload,
  }),
};
