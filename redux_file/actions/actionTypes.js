export const authActions = {
  getToken: 'AUTH_GET_TOKEN',
  tokenGranted: 'AUTH_DO_LOGIN',
  logout: 'AUTH_DO_LOGOUT',
  doRegister: 'AUTH_DO_REGISTER',
};

export const appState = {
  requesting: 'PAGE_IS_LOADING',
  loaded: 'PAGE_LOADED',
  requestFailed: 'REQUEST_FAILED',
};

export const marketTrans = {
  itemServed: 'MARKET_ITEM_SERVED',
  orderHistoryServed: 'MARKET_ORDER_HISTORY_SERVED',
  getItems: 'MARKET_GET_ITEM',
  reset: 'MARKET_ITEM_ROLLBACK',
  sendCheckout: 'MARKET_CHECKOUT',
  getOrderHistory: 'MARKET_ORDER_HISTORY',
  likesItem: 'USER_LIKES_ITEM',
  touch_order: 'USER_TOUCH_ORDER',
};

export const carts = {
  reset: 'MARKET_CART_RESET',
  refillCart: 'MARKET_CART_REFILL',
  updateCart: 'MARKET_CART_UPDATE',
  removeItemFromCart: 'MARKET_CART_REDUCE',
  add: 'MARKET_ADD_CART',
};
