import {apiUrl} from '../../config';

export async function getItem(payload) {
  try {
    let response = await fetch(apiUrl + 'item/getByVendor/1', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + payload.token,
      },
    });
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error(error);
  }
}

export async function push(payload) {
  try {
    let response = await fetch(`${apiUrl}${payload.kind}${payload.segment}`, {
      method: payload.method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + payload.token,
      },
      body: JSON.stringify(payload.body),
    });
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error(error);
  }
}
