import {apiUrl} from '../../config';

export async function _pushFormData(payload) {
  try {
    let response = await fetch(apiUrl + payload.target, {
      headers: {
        Accept: 'application/json',
      },
      method: 'POST',
      body: payload.body,
    });
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.error(error);
  }
}
