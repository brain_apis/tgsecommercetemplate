// // store files
import {createStore, combineReducers, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {
  persistStore,
  persistReducer,
  persistCombineReducers,
} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import {composeWithDevTools} from 'redux-devtools-extension';
import authReducer from './reducers/authReducer';
import appReducer from './reducers/appReducer';
import marketReducer from './reducers/marketReducer';
import cartReducer from './reducers/cartReducer';

import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1';

//saga
import rootSaga from './sagas/index';

const sagaMiddleware = createSagaMiddleware();

// combine reducer
// const rootReducer = combineReducers({
//   auth: authReducer,
//   app: appReducer,
//   market: marketReducer,
//   cart: cartReducer,
// });
const rootReducer = {
  auth: authReducer,
  app: appReducer,
  market: marketReducer,
  cart: cartReducer,
};

const persistConfig = {
  key: 'rootPersist',
  storage: AsyncStorage,
  whitelist: ['auth', 'cart'],
  stateReconciler: autoMergeLevel1,
  // debug: true,
};

const persistedReducer = persistCombineReducers(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);
