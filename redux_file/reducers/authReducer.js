import {authActions} from '../actions/actionTypes';

const initialState = {
  user_id: '',
  username: '',
  email: '',
  token: '',
  age: '',
  address: '',
  avatar:'https://icon-library.net/images/generic-user-icon/generic-user-icon-6.jpg',
  isAdmin: true,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case authActions.tokenGranted:
      // console.log(state.avatar, action.payload, 'reduce');
      return {
        ...state,
        user_id: action.payload.id,
        username: action.payload.name,
        email: action.payload.email,
        token: action.payload.token,
        avatar: action.payload.detail.avatar,
        address: action.payload.detail.address,
        age: action.payload.detail.age,
      };
    case authActions.logout:
      return initialState;
    default:
      return state;
  }
}
