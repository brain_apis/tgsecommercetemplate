import {carts} from '../actions/actionTypes';

const initialCart = {
  byIds: {},
  allIds: [],
};

export default function(state = initialCart, action) {
  switch (action.type) {
    case carts.add:
      let newIds;
      if (state.allIds.indexOf(action.payload.id) < 0) {
        newIds = [...state.allIds, action.payload.id];
      } else {
        newIds = state.allIds;
      }
      console.log(newIds, 'alids');
      return {
        byIds: {...state.byIds, [action.payload.id]: action.payload},
        allIds: [...newIds],
      };
    case carts.refillCart:
      let all = {};
      action.payload.forEach(i => (all[i.id] = i));
      return {
        byIds: all,
        allIds: Object.keys(all),
      };
    case carts.updateCart:
      if (state === initialCart) {
        return state;
      }
      return {
        byIds: action.payload,
        allIds: Object.keys(action.payload),
      };
    case carts.removeItemFromCart:
      state.allIds[action.payload.id] = undefined;
      return {
        byIds: state.byIds,
        allIds: Object.keys(state.byIds),
      };
    case carts.reset:
      return initialCart;
    default:
      return state;
  }
}
