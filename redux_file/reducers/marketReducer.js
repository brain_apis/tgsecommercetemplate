import {marketTrans} from '../actions/actionTypes';

const initialState = {
  shopname: 'SECRET SHOP',
  cart: [],
  tax: 2,
  items: [
    {
      id: 99128,
      item_name: 'dump',
      description: 'it is visible if no item found',
      price: 999,
      vendor: 'casper',
      qty: 1,
      item_image:'https://www.oatey.com//ASSETS/IMAGES/ITEMS/DETAIL_PAGE/NoImage.png',
    },
  ],
  orderHistory: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case marketTrans.updateCart:
      return {
        ...state,
        cart: [...action.payload],
      };
    case marketTrans.removeItemFromCart:
      return {
        ...state,
        cart: state.cart.filter(i => i.id !== action.payload.id),
      };
    case marketTrans.orderHistoryServed:
      // return false;
      return {
        ...state,
        orderHistory: action.payload,
      };
    case marketTrans.itemServed:
      return {
        ...state,
        items: action.payload,
      };
    case marketTrans.fillCart:
      if (state.cart.find(i => i.id === action.payload.id)) {
        return state;
      } else {
        return {
          ...state,
          cart: [...state.cart, action.payload],
        };
      }
    case marketTrans.emptyCart:
      return {
        ...state,
        cart: [],
      };
    case marketTrans.reset:
      return initialState;
    default:
      return state;
  }
}
