import {takeEvery, takeLatest, call, put} from 'redux-saga/effects';
import {authActions, appState} from '../actions/actionTypes';
import {navigatorRef} from './../../App';
import {NavigationActions} from 'react-navigation';
// import AsyncStorage from '@react-native-community/async-storage';

import {requestToken, push} from '../api/authApi';
import {_pushFormData} from '../api/multipartApi';
import {Alert} from 'react-native';

export const authSaga = [
  takeLatest(authActions.getToken, loginWorker),
  takeLatest(authActions.doRegister, regWorker),
];

function* loginWorker(action) {
  console.log(action, 'login worker act');
  const response = yield call(requestToken, action.payload);
  console.log(response, 'login worker response');
  // yield put({type: appState.loaded});
  if (response.status === true) {
    yield put({type: authActions.tokenGranted, payload: response.data});
    // console.log('true');
    navigatorRef.dispatch(
      NavigationActions.navigate({
        routeName: 'Shop',
      }),
    );
  } else if (response.status === false) {
    yield put({
      type: appState.requestFailed,
      payload: {
        loading: false,
        message: response.message,
      },
    });
    Alert.alert('Gagal Login', response.message);
  }
}

function* regWorker(action) {
  action.payload.target = 'user/register';
  const response = yield call(_pushFormData, action.payload);
  console.log(response, 'act reg');

  if (response.status === true) {
    yield put({type: appState.loaded});
    Alert.alert(
      'Bagus, pendaftaran berhasil',
      `username :${response.data.username} dan password :${response.data.password} `,
      [
        {
          text: 'OK',
          onPress: () => {
            Alert.alert('Follow-up', 'Langsung masuk ?', [
              {
                text: 'Ya',
                onPress: () => {
                  navigatorRef.dispatch(
                    NavigationActions.navigate({
                      routeName: 'Shop',
                    }),
                  );
                },
              },
              {
                text: 'Tidak',
                onPress: () => {
                  navigatorRef.dispatch(
                    NavigationActions.navigate({
                      routeName: 'RegisterStep1',
                    }),
                  );
                },
              },
            ]);
          },
        },
      ],
    );
  } else if (response.status === false || response.errors.name.length > 0) {
    // error by laravel
    if (response.errors.name.length > 0) {
      Alert.alert('Pendaftaran gagal', response.errors.name[0]);
    } else {
      Alert.alert('Pendaftaran gagal', response.message);
    }
    yield put({
      type: appState.requestFailed,
      payload: {
        loading: false,
        message: response.message,
      },
    });

    navigatorRef.dispatch(
      NavigationActions.navigate({
        routeName: 'RegisterStep1',
      }),
    );
  }
}
