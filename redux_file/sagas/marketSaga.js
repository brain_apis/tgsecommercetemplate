import {takeEvery, takeLatest, call, put} from 'redux-saga/effects';
import {appState, marketTrans, carts} from '../actions/actionTypes';

import {getItem, push} from '../api/marketApi';
import {Alert} from 'react-native';

export const marketSaga = [
  takeLatest(marketTrans.getItems, getItemWorker),
  takeLatest(marketTrans.sendCheckout, sendCheckoutWorker),
  takeLatest(marketTrans.getOrderHistory, getHistoryOrderWorker),
  takeLatest(marketTrans.likesItem, likingWorker),
  takeLatest(marketTrans.touch_order, touch_orderWorker),
];

function* getItemWorker(action) {
  // console.log(action.payload, 'getItem worker act');
  // return;
  const response = yield call(getItem, action.payload);
  // console.log(response, 'getItem worker response');

  if (response.status === false) {
    put({
      type: appState.requestFailed,
      payload: {
        loading: false,
        message: response.message,
      },
    });
    Alert.alert('Item gagal diambil', response.message);
  } else {
    yield put({type: marketTrans.itemServed, payload: response.data});
    yield put({type: appState.loaded});
  }
}

function* sendCheckoutWorker(action) {
  // return;
  action.payload.method = 'post';
  action.payload.kind = 'order';
  action.payload.segment = '';
  const response = yield call(push, action.payload);
  // console.log(response, 'getItem worker response');

  if (response.status === true) {
    yield put({type: carts.reset});
    yield put({type: appState.loaded});
    Alert.alert('Pesan', 'Checkout berhasil !');
  } else {
    put({
      type: appState.requestFailed,
      payload: {
        loading: false,
        message: response.message,
      },
    });
    Alert.alert('Checkout gagal', response.message);
    // yield put({type: marketTrans.itemServed, payload: response.data});
  }
}
function* getHistoryOrderWorker(action) {
  action.payload.method = 'GET';
  action.payload.kind = 'user';
  action.payload.segment = '/orderHistory/' + action.payload.id;
  const response = yield call(push, action.payload);

  if (response.status === true) {
    yield put({type: marketTrans.orderHistoryServed, payload: response.data});
    // alert('berhasil');
  } else {
    put({
      type: appState.requestFailed,
      payload: {
        loading: false,
        message: response.message,
      },
    });
    Alert.alert('History gagal diambil', response.message);
    // yield put({type: marketTrans.itemServed, payload: response.data});
  }
}

function* likingWorker(action) {
  action.payload.method = 'POST';
  action.payload.token = 'no need';
  action.payload.kind = 'user';
  action.payload.segment = '/like/' + action.user_id;
  const response = yield call(push, action.payload);

  if (response.status === true) {
    Alert.alert('favoriter !', response.message);
  }
}

function* touch_orderWorker(action) {
  action.payload.method = 'PATCH';
  action.payload.token = 'no need';
  action.payload.kind = 'order';
  action.payload.segment = '/' + action.order_id;
  const response = yield call(push, action.payload);
  if (response.status === true) {
    yield call(getHistoryOrderWorker, {payload: {id: action.user_id}});
    Alert.alert('Info', action.returnMessage + ' berhasil !');
  } else {
    Alert.alert('Error', 'Order worker error');
  }
}
